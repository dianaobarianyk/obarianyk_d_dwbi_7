USE [labor_sql]
GO

--1
SELECT ROW_NUMBER() OVER(ORDER BY id_comp, trip_no) num, trip_no, id_comp
FROM Trip T;

--2
SELECT ROW_NUMBER() OVER(PARTITION BY id_comp ORDER BY trip_no) num,
   trip_no, id_comp
FROM Trip T;

--3 
SELECT model, color, type, price FROM 
(SELECT *, MIN(price) OVER ( PARTITION BY type, color) AS min_price
FROM Printer) A
WHERE price = min_price;


--4
SELECT DISTINCT maker FROM 
(SELECT maker, COUNT(model) OVER(PARTITION BY maker) count_model 
FROM Product
WHERE type= 'PC') A
WHERE A.count_model >2;

--5 
SELECT price FROM
(SELECT DISTINCT price, ROW_NUMBER() OVER(ORDER BY price DESC) n
FROM PC) A
WHERE n=2;


--6
SELECT *,
 NTILE(3) OVER (ORDER BY SUBSTRING(Name,CHARINDEX(' ',Name) +1 ,LEN(Name))) 
 FROM Passenger P;

--7
SELECT *, ROW_NUMBER() OVER(ORDER BY price DESC) id,
      COUNT(*) OVER() row_total,
	  NTILE(CAST((SELECT CEILING(COUNT(*)/3.0) FROM PC) AS int))
	  OVER(ORDER BY price DESC) page_num,
	  (SELECT CEILING(COUNT(*)/3.0) FROM PC) page_total
FROM PC;


--8
SELECT max_sum, type, date, point FROM
(SELECT *, max(s) OVER(ORDER BY s DESC) max_sum  FROM 
			(SELECT point, date, type= 'inc', inc s
			FROM Income
			UNION ALL
			SELECT point, date, type= 'inc', inc s
			FROM Income_o
			UNION ALL
			SELECT point, date, type= 'out', out s
			FROM Outcome
			UNION ALL
			SELECT point, date, type= 'out', out s
			FROM Outcome_o) A
 ) B
 WHERE s= max_sum;


 --9
 SELECT *, price-AVG(price) OVER (PARTITION BY speed) dif_local_price,
           price-AVG(price) OVER () dif_tocal_price,
		   AVG(price) OVER () total_price
 FROM PC
 ORDER BY speed;