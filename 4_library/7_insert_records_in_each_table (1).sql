USE [D_Obarianyk_Library]
GO

INSERT INTO [Library].[Authors]
([Name], [URL], [inserted], [inserted_by])
VALUES ('Sylvia S. Mader','www.sylviamader.com', GETDATE(), user_name()),
('Jan Gulberg', 'www.jan_gulberg.com',GETDATE(), user_name()),
('Bill Bryson', 'www.billbryson.com',GETDATE(), user_name()),
('Stephen Hawking', 'www.stepanhawking.com',GETDATE(), user_name()),
('Kate Hannigan', 'www.katehahnigan.com',GETDATE(), user_name()),
('Thomas Sowell', 'www.thomassowell.com',GETDATE(), user_name()),
('Paul Krugman', 'www.paulkrugman.com',GETDATE(), user_name()),
('Inc. BarCharts', 'www.barcharts.com',GETDATE(), user_name())
GO




INSERT INTO [Library].[Publishers]
([Name], [URL], [inserted], [inserted_by])
VALUES ('McGraw-Hill Education','www.mcgraw_hilleducation.com', GETDATE(), user_name()),
('W. W. Norton&Company','www.nortonandcompany.com', GETDATE(), user_name()),
('Broadway Books', 'www.broadwaybooks.com',GETDATE(), user_name()),
('Bantam', 'www.bantam.com',GETDATE(), user_name()),
('Little, Brown Books', 'www.littlebrownbooks.com',GETDATE(), user_name()),
('Basics Books', 'www.basicsbooks.com',GETDATE(), user_name()),
('Worth Publishers', 'www.worthpublishers.com',GETDATE(), user_name()),
('QuickStudy', 'www.quickstudy.com', GETDATE(), user_name())

GO

INSERT INTO [Library].[Books]
([ISBN], [Publisher_Id],[URL], [Price], [inserted], [inserted_by])
VALUES ('0078024269', 1, 'www.biologysmader.com', 25, GETDATE(), user_name()),
('039304002X', 2, 'www.mathematics.com', 40, GETDATE(), user_name()),
('076790818X', 3, 'www.ashorthistory.com', 15, GETDATE(), user_name()),
('0553380168', 4, 'www.abriefhistoryoftime.com',55, GETDATE(), user_name()),
('0316403490', 5, 'www.detective_assistant.com', 30, GETDATE(), user_name()),
('046506730', 6, 'www.basiceconomics.com', 45, GETDATE(), user_name()),
('1464143846', 7, 'www.economics.com', 20, GETDATE(), user_name()),
('14232119627', 8, 'www.psychology.com', 30, GETDATE(), user_name())
GO

INSERT INTO [Library].[BooksAuthors]
([BooksAuthors_id],[ISBN], [Author_Id],[Seq_No], [inserted], [inserted_by])
VALUES (1,'0078024269', 1, 1 , GETDATE(), user_name()),
(2,'039304002X', 2, 2, GETDATE(), user_name()),
(3,'076790818X', 3, 3, GETDATE(), user_name()),
(4,'0553380168', 4, 4, GETDATE(), user_name()),
(5,'0316403490', 5, 5, GETDATE(), user_name()),
(6,'046506730', 6, 6, GETDATE(), user_name()),
(7,'1464143846', 7, 7, GETDATE(), user_name()),
(8,'14232119627', 8, 8, GETDATE(), user_name())
GO 



SELECT * FROM [Library].[Authors]
SELECT * FROM [Library].[Books]
SELECT * FROM [Library].[BooksAuthors]
SELECT * FROM [Library].[Publishers]

SELECT * FROM [Library].[Authors_log]

