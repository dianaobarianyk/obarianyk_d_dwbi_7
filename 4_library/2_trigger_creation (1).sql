USE [D_Obarianyk_Library]
GO

DROP TRIGGER IF EXISTS [Library].[Authors_update]
GO

DROP TRIGGER IF EXISTS [Library].[Books_update]
GO

DROP TRIGGER IF EXISTS [Library].[BooksAuthors_update]
GO

DROP TRIGGER IF EXISTS [Library].[Publishers_update]
GO

DROP TRIGGER IF EXISTS [Library].[Authors_tr]
GO

CREATE TRIGGER [Library].[Authors_update]
ON [Library].[Authors]
AFTER UPDATE 
AS 
	UPDATE [Library].[Authors]
	SET updated = GETDATE(),
	    updated_by = user_name()
	 WHERE Author_id IN 
	(SELECT DISTINCT Author_id FROM [inserted])
GO

CREATE TRIGGER [Library].[Books_update]
ON [Library].[Books]
AFTER UPDATE 
AS 
	UPDATE [Library].[Books]
	SET updated = GETDATE(),
	    updated_by = user_name()
	 WHERE ISBN IN 
	(SELECT DISTINCT ISBN FROM [inserted])
GO

CREATE TRIGGER [Library].[BooksAuthors_update]
ON [Library].[BooksAuthors]
AFTER UPDATE 
AS 
	UPDATE [Library].[BooksAuthors]
	SET updated = GETDATE(),
	    updated_by = user_name()
	 WHERE [BooksAuthors_id] IN 
	(SELECT DISTINCT [BooksAuthors_id] FROM [inserted])
GO


CREATE TRIGGER [Library].[Publishers_update]
ON [Library].[Publishers]
AFTER UPDATE 
AS 
	UPDATE [Library].[Publishers]
	SET updated = GETDATE(),
	    updated_by = user_name()
	 WHERE Publisher_Id IN 
	(SELECT DISTINCT Publisher_Id FROM [inserted])
GO


CREATE TRIGGER [Library].[Authors_tr]
ON [Library].[Authors]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 
---- insert
if @operation = 'I'
begin
	insert into [Library].[Authors_log]
				(operation_type, [Author_Id_new], [Name_new],[URL_new])
	Select 
	    @operation,
		[Library].[Authors].[Author_Id], 
		[Library].[Authors].[Name], 
		[Library].[Authors].[URL]
	FROM [Library].[Authors]
	inner join inserted 
	on [Library].[Authors].[Author_id] = inserted.[Author_id]
end
---- delete
if @operation = 'D'
begin
	insert into [Library].[Authors_log]
	(operation_type, [Author_Id_old], [Name_old],[URL_old])
	Select 
		 @operation,
		[Library].[Authors].[Author_Id], 
		[Library].[Authors].[Name], 
		[Library].[Authors].[URL]
	FROM [Library].[Authors]
	inner join deleted 
	on [Library].[Authors].[Author_id] = deleted.[Author_id]
end
	 

--- update
if @operation = 'U'
	begin
			insert into [Library].[Authors_log]
				([operation_type],
				[Author_Id_new],
				 [Name_new],
				 [URL_new],
				 [Author_Id_old],
				 [Name_old],
				 [URL_old])
			select
				@operation,
				inserted.[Author_Id],
				inserted.[Name],
				inserted.[URL],
				deleted.[Author_Id],
		        deleted.[Name],
				deleted.[URL]
				
		
			FROM [Library].[Authors]
				inner join  inserted on [Library].[Authors].[Author_id] = inserted.[Author_id]
				inner join deleted on [Library].[Authors].[Author_id] = deleted.[Author_id]
	end
END
GO

