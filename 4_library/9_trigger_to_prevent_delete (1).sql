USE [D_Obarianyk_Library]
GO

DROP TRIGGER IF EXISTS [Library].[tr_prevent_delete_from_Authors_log]
GO

CREATE TRIGGER [Library].[tr_prevent_delete_from_Authors_log]
ON [Library].[Authors_log]
INSTEAD OF DELETE
AS

SET NOCOUNT ON

DECLARE @ERROR_MESSAGE VARCHAR(250)

SELECT @ERROR_MESSAGE =
       'Delete not allowed on table [Library].[Authors_log]'

RAISERROR (@ERROR_MESSAGE, 16,1)

IF @@TRANCOUNT > 0
BEGIN ROLLBACK
END

RETURN
GO