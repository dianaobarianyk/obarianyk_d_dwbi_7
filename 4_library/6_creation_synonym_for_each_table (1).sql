USE [D_Obarianyk_Library_synonym]
GO



CREATE SYNONYM [dbo].[Authors] 
FOR [D_Obarianyk_Library].[Library].[Authors]
GO

CREATE SYNONYM [dbo].[Books] 
FOR [D_Obarianyk_Library].[Library].[Books]
GO

CREATE SYNONYM [dbo].[BooksAuthors] 
FOR [D_Obarianyk_Library].[Library].[BooksAuthors]
GO

CREATE SYNONYM [dbo].[Publishers] 
FOR [D_Obarianyk_Library].[Library].[Publishers]
GO

CREATE SYNONYM [dbo].[Authors_log] 
FOR [D_Obarianyk_Library].[Library].[Authors_log]
GO






