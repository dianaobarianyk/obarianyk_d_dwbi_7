USE [D_Obarianyk_Library_view]
GO

--- create view
CREATE OR ALTER VIEW [Authors_view] 
(
    [Author_Id],
    [Name],
    [URL],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
  )
  AS
  SELECT 
    
	   [Author_Id],
       [Name],
       [URL],
       [inserted],
       [inserted_by],
	   [updated],
	   [updated_by]

  FROM [D_Obarianyk_Library].[Library].[Authors]
GO

CREATE OR ALTER VIEW [Publishers_view] 
(
    [Publisher_Id],
    [Name],
    [URL],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
  )
  AS
  SELECT 
    
	   [Publisher_Id],
       [Name],
       [URL],
       [inserted],
       [inserted_by],
	   [updated],
	   [updated_by]

  FROM [D_Obarianyk_Library].[Library].[Publishers]
GO

CREATE OR ALTER VIEW [Books_view] 
(
    [ISBN],
    [URL],
	[Price],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
  )
  AS
  SELECT 
    
	   [ISBN],
       [URL],
	   [Price],
       [inserted],
       [inserted_by],
	   [updated],
	   [updated_by]

  FROM [D_Obarianyk_Library].[Library].[Books]
GO

CREATE OR ALTER VIEW [BooksAuthors_view] 
(
    [BooksAuthors_id],
    [ISBN],
	[Author_Id],
	[Seq_No],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
  )
  AS
  SELECT 
       [BooksAuthors_Id],
	   [ISBN],
       [Author_Id],
	   [Seq_No],
       [inserted],
       [inserted_by],
	   [updated],
	   [updated_by]

  FROM [D_Obarianyk_Library].[Library].[BooksAuthors]
GO

CREATE OR ALTER VIEW [Authors_log_view] 
(   [operation_id],
    [Author_Id_new],
    [Name_new],
	[URL_new],
	[Author_Id_old],
    [Name_old],
	[URL_old],
	[operation_type],
	[operation_datetime]
  )
  AS
  SELECT 
	[operation_id],
    [Author_Id_new],
    [Name_new],
	[URL_new],
	[Author_Id_old],
    [Name_old],
	[URL_old],
	[operation_type],
	[operation_datetime]

  FROM [D_Obarianyk_Library].[Library].[Authors_log]
GO