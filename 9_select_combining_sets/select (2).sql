use labor_sql
go


--1
;WITH CTE_region(region_id) AS
(SELECT region_id FROM [geography])
SELECT * FROM CTE_region;


--2
;WITH 
CTE_pc(model, ram) AS
(SELECT model, ram FROM [PC] WHERE speed> 500),
CTE_pc_ram (model) AS
(SELECT distinct model FROM CTE_pc WHERE ram=128)
SELECT * FROM CTE_pc_ram;


--3 
;WITH CTE_region(region_id, place_ID, name, Placelevel) AS
(SELECT g.[region_id], g.id place_ID, g.name, Placelevel=0  FROM [geography] g
WHERE g.[region_id] IS NULL
UNION ALL
SELECT g.[region_id], g.id place_ID, g.name, Placelevel +1
FROM [geography] g INNER JOIN  CTE_region  c
ON g.[region_id]=c.[place_ID])
SELECT * FROM CTE_region
WHERE Placelevel =1;


--4
;WITH CTE1 AS
(SELECT g.[region_id], g.id place_ID, g.name, Placelevel1=0  FROM [geography] g
WHERE region_id=4
UNION ALL
SELECT g.[region_id], g.id place_ID, g.name, Placelevel1 +1
 FROM [geography] g INNER JOIN  CTE1 c
 ON g.[region_id]=c.[place_ID]
)
SELECT * FROM CTE1;

--5
;WITH CTE_num(n) AS
(SELECT n=1 
UNION ALL
SELECT n+1 FROM CTE_num
WHERE n< 10000)

SELECT n FROM CTE_num
OPTION (MAXRECURSION 10000);

--6
;WITH CTE_num2(n) AS
(SELECT n=1 
UNION ALL
SELECT n+1 FROM CTE_num2
WHERE n< 100000)

SELECT n FROM CTE_num2
OPTION (MAXRECURSION 0);

--7
DECLARE @start_date date
DECLARE @end_date date
SET @start_date = '2018.01.01'
SET @end_date = '2018.12.31'

;WITH CTE_date AS
(SELECT 1 AS id, 
@start_date AS start_date,
DATENAME(WEEKDAY, @start_date) AS  day_name
UNION ALL
SELECT [CTE_date].[id] +1 AS id,
DATEADD(DAY, 1, [CTE_date].[start_date]) AS start_date,
DATENAME(WEEKDAY, DATEADD(DAY, 1, [CTE_date].[start_date])) AS day_name
FROM CTE_date
WHERE DATEADD(DAY, 1, [CTE_date].[start_date]) < @end_date)

SELECT COUNT(day_name) FROM CTE_date
WHERE day_name IN  ('Saturday', 'Sunday')
OPTION(MAXRECURSION 366);

--8 
SELECT DISTINCT maker FROM Product 
WHERE (maker  NOT IN (SELECT maker FROM Product WHERE type ='Laptop'))
AND type ='PC';

--9 
SELECT DISTINCT maker FROM Product 
WHERE (maker != ALL (SELECT maker FROM Product WHERE type ='Laptop'))
AND type ='PC';

--10
SELECT DISTINCT maker FROM Product 
WHERE NOT (maker = ANY (SELECT maker FROM Product WHERE type ='Laptop'))
AND type ='PC';


--11
SELECT DISTINCT maker FROM Product 
WHERE (maker  IN (SELECT maker FROM Product WHERE type ='PC'))
AND type ='Laptop';

--12
SELECT DISTINCT maker FROM Product 
WHERE type ='PC' 
AND  NOT (maker != ALL (SELECT maker FROM Product WHERE type ='laptop'));

--13 
SELECT DISTINCT maker FROM Product 
WHERE (maker = ANY (SELECT maker FROM Product WHERE type ='PC'))
AND type ='Laptop';

--14
SELECT DISTINCT maker FROM Product 
WHERE  maker = ANY (SELECT DISTINCT maker FROM Product 
                WHERE  model IN (SELECT model FROM PC))
AND maker != ALL (SELECT DISTINCT maker FROM Product 
WHERE  model NOT IN (SELECT model FROM PC) AND type= 'PC');

--15 
IF EXISTS(SELECT country FROM Classes WHERE country= 'Ukraine')
    SELECT class FROM Classes 
    WHERE country = 'Ukraine'
ELSE
   SELECT country, class 
   FROM  Classes C;

 
 --16
SELECT O.ship, O.battle, B.date 
FROM Outcomes O
JOIN Battles B
ON B.name = O.battle
WHERE O.ship IN  (SELECT O1.ship FROM Outcomes O1
                  JOIN Battles B1 ON B1.name = O1.battle
                  WHERE O1.result = 'damaged' AND DATEDIFF( D,B.date,B1.date) < 0)
AND O.result ='OK';

 --17
 SELECT DISTINCT maker FROM Product PR
 WHERE maker NOT IN (SELECT DISTINCT maker FROM Product P
                     WHERE P.type= 'PC' AND NOT EXISTS (SELECT * FROM PC
                                                        WHERE PC.model = P.model) )
 AND type = 'PC';

 --18
 SELECT maker FROM Product PR
 WHERE ( model  IN (SELECT model FROM PC WHERE speed= (SELECT MAX(speed) FROM PC))) 
 AND (maker IN (SELECT maker FROM Product WHERE type = 'Printer'));
  
  --19 
  SELECT S.class FROM Outcomes O
  JOIN Ships S ON S.name = O.ship
  WHERE O.result ='sunk'
  GROUP BY S.class
  HAVING COUNT(O.ship) >=1
  UNION 
  SELECT C.class FROM Classes C
  JOIN Outcomes O ON O.ship = C.class
  WHERE O.result = 'sunk'
  GROUP BY C.class
  HAVING COUNT(O.ship) >=1;


  --20
  SELECT model, price FROM Printer
  WHERE price = (SELECT MAX(price) FROM Printer);

  --21 
  SELECT PR.type, L.model, L.speed
  FROM Laptop L
  INNER JOIN Product PR 
  ON L.model = PR.model
  WHERE speed < ALL (SELECT speed FROM PC);

  --22 
  SELECT PR.maker, price FROM Printer
  JOIN Product PR 
  ON PR.model = Printer.model
  WHERE price = (SELECT MIN(price) FROM Printer WHERE color = 'y') 
  AND color = 'y';

  --23
  SELECT * FROM 
  (SELECT O.battle, C.country, COUNT(O.ship) count_s
  FROM Outcomes O, Classes C, Ships S
  WHERE O.ship= S.name AND C.class = S.class
  GROUP BY O.battle, C.country) R
  WHERE R.[count_s] >=2;
   

   --24
   SELECT maker, 
  (SELECT COUNT(*) FROM PC INNER JOIN Product PR
   ON PC.model = PR.model WHERE PR.maker= P.maker) AS pc,
  (SELECT COUNT(*) FROM Laptop L INNER JOIN Product PR
   ON L.model = PR.model WHERE PR.maker= P.maker) AS laptop,
   (SELECT COUNT(*) FROM Printer PRN  INNER JOIN Product PR
   ON PRN.model = PR.model WHERE PR.maker= P.maker) AS printer
   FROM Product P
   GROUP BY maker;

   --25
SELECT P.maker, 
  CASE WHEN (SELECT COUNT(PC.model) FROM PC INNER JOIN Product PR
   ON PC.model = PR.model WHERE PR.maker= P.maker) > 0
   THEN 'yes(' + CAST((SELECT COUNT(PC.model) FROM PC INNER JOIN Product PR
   ON PC.model = PR.model WHERE PR.maker= P.maker) as varchar(20)) + ')'
   ELSE 'no' 
  END pc
FROM Product P
GROUP BY P.maker;

--26
SELECT O.point, O.date, I.inc, O.out 
FROM Outcome_o O
LEFT JOIN Income_o I
ON O.date= I.date AND O.point=I.point
UNION
SELECT I.point, I.date, I.inc, O.out 
FROM Outcome_o O
RIGHT JOIN Income_o I
ON O.date= I.date AND O.point=I.point ;


--27
SELECT B.name, B.numGuns, B.bore, B.displacement, B.type,
B.country, B.launched, B.class FROM 
(SELECT S.name, C.numGuns, C.bore, C.displacement, C.type,
C.country, S.launched, S.class,
CASE WHEN C.numGuns= 8 THEN 1 ELSE 0 END AS A1,
CASE WHEN C.bore= 15 THEN 1 ELSE 0 END AS A2,
CASE WHEN C.displacement= 32000 THEN 1 ELSE 0 END AS A3,
CASE WHEN C.type= 'bb' THEN 1 ELSE 0 END AS A4,
CASE WHEN C.country= 'USA' THEN 1 ELSE 0 END AS A5,
CASE WHEN S.launched= 1915 THEN 1 ELSE 0 END AS A6,
CASE WHEN S.class= 'Kon' THEN 1 ELSE 0 END AS A7
FROM Ships S
INNER JOIN  Classes C
ON S.class= C.class) B
WHERE (B.A1 + B.A2 + B.A3 + B.A4 + B.A5 + B.A6 + B.A7) >= 4;

--28
SELECT OO.point, OO.date,
CASE WHEN OO.out > A.sum_out THEN 'once a day' 
     WHEN OO.out = A.sum_out THEN 'both'
	 ELSE 'more than once a day' END AS result 
FROM (SELECT O.point, O.date, SUM(O.out) AS sum_out
FROM Outcome O
GROUP BY date, point) A
FULL JOIN outcome_o OO
ON A.date= OO.date and A.point = OO.point;
 
--29
SELECT PR.maker, PR.model, PR.type, PC.price
FROM Product PR
INNER JOIN PC ON PR.model = PC. model
WHERE PR.maker='B'
UNION ALL
SELECT PR.maker, PR.model, PR.type, L.price
FROM Product PR
INNER JOIN Laptop L ON PR.model = L.model
WHERE PR.maker='B'
UNION ALL
SELECT PR.maker, PR.model, PR.type, P.price
FROM Product PR
INNER JOIN Printer P ON PR.model = P.model
WHERE PR.maker='B';

--30
SELECT S.name, C.class
FROM Ships S
INNER JOIN Classes C ON S.name = C.class
UNION  
SELECT O.ship, C.class
FROM Outcomes O
INNER JOIN Classes C ON O.ship = C.class;


--31
SELECT cl AS class  FROM
(SELECT class AS cl,COUNT(name) AS co
 FROM Ships 
 GROUP BY class
 UNION ALL
 SELECT C.class AS cl, COUNT(O.ship) AS co
 FROM Outcomes O
 INNER JOIN Classes C ON O.ship = C.class
 GROUP BY  class) A
GROUP BY cl
HAVING SUM(co)=1;

--32
SELECT S.name AS ship_name
FROM Ships S
WHERE S.launched < 1942
UNION 
SELECT O.ship AS ship_name
FROM Outcomes O
INNER JOIN Battles B ON O.battle = B.name
WHERE DATEPART(YEAR, B.date) <1942
ORDER BY ship_name;