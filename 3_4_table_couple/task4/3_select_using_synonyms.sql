USE [education]
GO

SELECT first_name, email FROM [D_Obarianyk].[customer]
WHERE last_name='Starchak'

SELECT customer_id, street, city, country
FROM [D_Obarianyk].[customer]

SELECT customer_id, first_name, last_name, phone_number
FROM [D_Obarianyk].[customer_details]
WHERE operation_type = 'I';

SELECT customer_id, first_name, last_name, operation_type
FROM [D_Obarianyk].[customer_details]