USE [D_O_module_3]
GO


INSERT INTO [db].[student](
		group_id,
		first_name,
		last_name,
		student_number,
		gender,
		date_of_birth,
		phone_number,
		email,
		updated_date)
VALUES( 1,
		'Maria',
		'Perlova',
		'12312300',
		'female',
		'1996.12.23',
		'0971234567',
		'mpervola@gmail.com',
		'2018.07.12')
GO
