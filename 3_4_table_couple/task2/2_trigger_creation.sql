USE [D_O_module_3]
GO

DROP TRIGGER IF EXISTS [db].[tr_customer_insert] 
GO

DROP TRIGGER IF EXISTS [db].[tr_customer_delete] 
GO

DROP TRIGGER IF EXISTS [db].[tr_customer_update] 
GO

---- trigger creation
---insert
CREATE TRIGGER [db].[tr_customer_insert]
ON [db].[customer]
AFTER INSERT
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 

if @operation = 'I'
begin
	insert into [db].[customer_details]
				( operation_type,
				 operation_date, 
				 customer_id,
				 first_name,
				  last_name,
				  gender,
				  date_of_birth,
				  phone_number,
				  email,
				  street,
				  city,
				  country)
	Select 
		@operation, 
		GETDATE(),
		customer.[customer_id],
		customer.[first_name],
		customer.[last_name],
		customer.[gender],
		customer.[date_of_birth],
		customer.[phone_number],
		customer.[email],
		customer.[street],
		customer.[city],
		customer.[country]

	FROM [db].[customer]
	INNER JOIN inserted ON customer.customer_id =inserted.customer_id
end
END 
GO


----trigger creation
---- delete
CREATE OR ALTER TRIGGER [db].[tr_customer_delete]
ON [db].[customer]
AFTER DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 

if @operation = 'D'
begin
	insert into [db].[customer_details]
			( operation_type,
				 operation_date, 
				 customer_id,
				 first_name,
				  last_name,
				  gender,
				  date_of_birth,
				  phone_number,
				  email,
				  street,
				  city,
				  country)
	Select 
		@operation, 
		GETDATE(),
		customer.[customer_id],
		customer.[first_name],
		customer.[last_name],
		customer.[gender],
		customer.[date_of_birth],
		customer.[phone_number],
		customer.[email],
		customer.[street],
		customer.[city],
		customer.[country]

	FROM [db].[customer]
	INNER JOIN deleted ON customer.customer_id = deleted.customer_id
				
end
END
GO


---trigger creation
--- update
CREATE TRIGGER [db].[tr_customer_update]
ON [db].[customer]
AFTER UPDATE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 
if @operation = 'U'
	begin
			insert into [db].[customer_details] 
				(operation_type,
				 operation_date, 
				 customer_id,
				 first_name,
				  last_name,
				  gender,
				  date_of_birth,
				  phone_number,
				  email,
				  street,
				  city,
				  country)
	Select 
		@operation, 
		GETDATE(),
		customer.[customer_id],
		customer.[first_name],
		customer.[last_name],
		customer.[gender],
		customer.[date_of_birth],
		customer.[phone_number],
		customer.[email],
		customer.[street],
		customer.[city],
		customer.[country]
			FROM [db].[customer]
				inner join  inserted on [db].[customer].customer_id = inserted.customer_id
				inner join deleted on [db].[customer].customer_id = deleted.customer_id
	end

END
GO