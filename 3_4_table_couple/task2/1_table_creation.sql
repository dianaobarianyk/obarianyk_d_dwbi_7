USE [D_O_module_3]
GO

DROP TABLE IF EXISTS [db].[customer]
GO

DROP TABLE IF EXISTS [db].[customer_details]
GO

CREATE TABLE [db].[customer](
 customer_id	    int IDENTITY NOT NULL PRIMARY KEY,
 first_name			varchar(20) NOT NULL,
 last_name			varchar(20) NOT NULL,
 gender				varchar(10) NOT NULL,
 date_of_birth		date        NOT NULL,
 phone_number		varchar(10) NOT NULL,
 email				varchar(20) UNIQUE NOT NULL,
 street             varchar(20) NOT NULL,
 city               varchar(20) NOT NULL,
 country			varchar(20) NOT NULL
);
GO

CREATE TABLE [db].[customer_details](
 customer_id		int  NOT NULL PRIMARY KEY,
 first_name			varchar(20),
 last_name			varchar(20),
 gender				varchar(10),
 date_of_birth		date ,
 phone_number		varchar(10),
 email				varchar(20),
 street             varchar(20),
 city               varchar(20),
 country			varchar(20),
 operation_type     varchar(20),
 operation_date     datetime

);
GO