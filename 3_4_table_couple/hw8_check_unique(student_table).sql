USE [D_O_module_3]
GO
--check unique 
-- we can't insert 2 people with the same student_number

INSERT INTO [db].[student](
		group_id,
		first_name,
		last_name,
		student_number,
		gender,
		date_of_birth,
		phone_number,
		email,
		updated_date)
VALUES( 1,
		'Olga',
		'Adamchyk',
		'12312300',
		'female',
		'1996.10.21',
		'0971888543',
		'oadamchyk@gmail.com',
		'2018.07.13')

GO