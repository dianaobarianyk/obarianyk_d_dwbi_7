USE [D_O_module_3]
GO
--check option
--we can't insert student with last_name = first_name

INSERT INTO [db].[student](
		group_id,
		first_name,
		last_name,
		student_number,
		gender,
		date_of_birth,
		phone_number,
		email,
		updated_date)
VALUES( 2,
		'Petr',
		'Petr',
		'22334499',
		'male',
		'1996.10.03',
		'0972227772',
		'petrpert@gmail.com',
		'2018.07.11')

GO