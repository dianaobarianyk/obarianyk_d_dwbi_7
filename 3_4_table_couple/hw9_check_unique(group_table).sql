USE [D_O_module_3]
GO
--check unique 
--we can't add 2 groups with the same short_name
 INSERT INTO [db].[group]
		( short_name,
		course,
		department,
		head_of_department,
		curator,
		university,
		additional_details)
VALUES('MTA',
		1,
		'Matematical history',
		'Kravchuk',
		'Damchenko',
		'Ivan Franko National University of Lviv',
		'Lviv'
		)