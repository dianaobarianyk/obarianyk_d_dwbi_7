USE [D_O_module_3]
GO

 INSERT INTO [db].[group]
		( short_name,
		course,
		department,
		head_of_department,
		curator,
		university,
		additional_details)
VALUES('MTA',
		1,
		'Matematical analysis',
		'Ivanov',
		'Petrov',
		'Ivan Franko National University of Lviv',
		'Lviv'
		)

 INSERT INTO [db].[group]
		( short_name,
		course,
		department,
		head_of_department,
		curator,
		university,
		additional_details, 
		inserted_date)
VALUES('MTE',
		1,
		'Matematical economics',
		'Vasylenko',
		'Sudorov',
		'Ivan Franko National University of Lviv',
		'Lviv',
		'2017.09.01')
GO

