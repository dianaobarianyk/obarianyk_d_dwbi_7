USE [D_O_module_3]
GO

DROP TABLE IF  EXISTS [db].[student]
GO

DROP TABLE IF  EXISTS [db].[group]
GO



CREATE TABLE [db].[group](
        group_id           int IDENTITY PRIMARY KEY, 
        short_name         varchar(10) UNIQUE,
		course             int,
		department         varchar(30),
		head_of_department varchar(30),
		curator            varchar(30),
		university         varchar(40),
		additional_details varchar(30),
		updated_date       date,
		inserted_date      date)
GO


CREATE TABLE [db].[student](
		student_id        int IDENTITY PRIMARY KEY,
		first_name        varchar(30),
		last_name         varchar(30),
		student_number    varchar(30) UNIQUE,
		gender            varchar(10),
		date_of_birth     date,
		phone_number      varchar(30),
		email             varchar(30),
		updated_date       date,
		inserted_date      date,
		group_id          int
		CONSTRAINT FK_group  FOREIGN KEY(group_id)
		REFERENCES [db].[group]([group_id]))
		GO

ALTER TABLE [db].[student]
ADD CONSTRAINT CHK_name 
CHECK (first_name != last_name)
GO