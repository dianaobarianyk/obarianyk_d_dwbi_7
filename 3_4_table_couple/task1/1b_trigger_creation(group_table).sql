USE [D_O_module_3]
GO

DROP TRIGGER IF EXISTS [db].[tr_group]
GO

---- trigger creation
CREATE TRIGGER [db].[tr_group] ON [db].[group]
AFTER  UPDATE
AS
 UPDATE [db].[group]
 SET updated_date = GETDATE()
 WHERE group_id IN (SELECT DISTINCT group_id FROM Inserted)

 
SELECT * FROM [db].[group]


--update one row
UPDATE [db].[group]
SET course = 2
WHERE short_name = 'MTE'

--updated date is modified using trigger
SELECT * FROM [db].[group]