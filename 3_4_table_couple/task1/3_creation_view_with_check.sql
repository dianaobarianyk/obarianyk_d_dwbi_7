USE [D_O_module_3]
GO

CREATE OR ALTER VIEW [db].[our_economics_group]
as
SELECT 
		[group_id],
		[short_name],
		[course],
		[department],
		[head_of_department],
		[curator],
		[university],
		[additional_details],
		[inserted_date],
		[updated_date] 
  FROM [db].[group]
  WHERE [short_name] = 'MTE'
  WITH CHECK OPTION
  GO

  SELECT * FROM [db].[our_economics_group]