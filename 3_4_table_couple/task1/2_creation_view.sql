USE [D_O_module_3]
GO

--- create view for group table
CREATE OR ALTER VIEW [db].[our_group] 
(		[group_id],
		[short_name],
		[course],
		[department],
		[head_of_department],
		[curator],
		[university],
		[additional_details],
		[inserted_date],
		[updated_date]
     
  )
  AS
  SELECT 
		[group_id],
		[short_name],
		[course],
		[department],
		[head_of_department],
		[curator],
		[university],
		[additional_details],
		[inserted_date],
		[updated_date]
  FROM [db].[group]
GO


SELECT * FROM [db].[our_group]

--- create view for student table

CREATE OR ALTER VIEW [db].[our_student] 
(		[student_id],
		[group_id],
		[first_name],
		[last_name],
		[student_number],
		[gender],
		[date_of_birth],
		[phone_number],
		[email],
		[inserted_date],
		[updated_date]
	)
	AS
	SELECT 
		[student_id],
	    [group_id],
		[first_name],
		[last_name],
		[student_number],
		[gender],
		[date_of_birth],
		[phone_number],
		[email],
		[inserted_date],
		[updated_date]
	FROM [db].[student]
GO

SELECT * FROM [db].[our_student]