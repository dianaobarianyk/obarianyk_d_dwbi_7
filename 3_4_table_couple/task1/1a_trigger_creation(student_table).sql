USE [D_O_module_3]
GO

DROP TRIGGER IF EXISTS [db].[tr_student]
GO

---- trigger creation
CREATE TRIGGER [db].[tr_student] ON [db].[student]
AFTER  UPDATE
AS
 UPDATE [db].[student]
 SET updated_date = GETDATE()
 WHERE student_id IN (SELECT DISTINCT student_id FROM Inserted)

 --insert new row
 INSERT INTO [db].[student](
		group_id,
		first_name,
		last_name,
		student_number,
		gender,
		date_of_birth,
		phone_number,
		email)
VALUES( 2,
		'Iryna',
		'Denchyk',
		'00012345',
		'female',
		'1996.08.03',
		'0931234567',
		'idenchyk@gmail.com')
GO

SELECT * FROM [db].[student]


--update one row
UPDATE [db].[student]
SET first_name = 'Mariana'
WHERE first_name = 'Iryna'

--updated date is modified using trigger
SELECT * FROM [db].[student]