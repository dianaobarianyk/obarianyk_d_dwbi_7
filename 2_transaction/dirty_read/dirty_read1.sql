use education
go

begin transaction

update dbo.details
set weight = 20
where detailid = 2

waitfor delay '00:00:10'

rollback transaction