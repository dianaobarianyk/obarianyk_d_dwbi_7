use education
go


begin transaction

select * 
from [dbo].[products]
where productid > 5

---- another transaction 
waitfor delay '00:00:10'

select * 
from [dbo].[products]
where productid > 5

commit transaction

