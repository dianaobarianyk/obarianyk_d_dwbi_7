use [people_ua_db]
go

drop table if exists [person]
go

drop table if exists [man_name]
go

drop table if exists [woman_name]
go

create table [person](
[person_id]		int		 identity(1,1),
[surname]	    nvarchar(20) not null,
[name]		    nvarchar(20) not null,
[sex]		    nchar(1) null,
[date_of_birth] date null,
[address_id]    int null,
constraint PK_person Primary Key ([person_id]),
constraint fk_address foreign key([address_id])
references [dbo].[address]([address_id])
)
go

create table [man_name](
[id]		 int		 identity(1,1)  Primary Key,
[name_id]	 int      not null,
)
go

insert into [man_name](name_id)
select id  from [dbo].[name_list_identity] 
where [name_list_identity].[sex] = 'm'

create table [woman_name](
[id]		 int		 identity(1,1)  Primary Key,
[name_id]	 int      not null,
)
go

insert into [woman_name](name_id)
select id  from [dbo].[name_list_identity] 
where [name_list_identity].[sex] = 'w'


declare [cursor_people] cursor for
select [surname], [sex], [amount] 
from [dbo].[surname_list_identity]
go

open [cursor_people]

declare @surname          nvarchar(20)
declare @sex              nchar(1)
declare @amount           int
declare @name             nvarchar(20)
declare @k                int
declare @m                int
declare @w                int
declare @date_of_birth    date
declare @address_id       int
declare @address_id_count int

fetch next from [cursor_people] into
@surname, @sex, @amount

while @@FETCH_STATUS=0

begin 
set @k = (select count(*) from [dbo].[name_list_identity]) 
set @m = (select count(*) from [dbo].[man_name]) 
set @w = (select count(*) from [dbo].[woman_name]) 

set @address_id_count=(select count(*) from [dbo].[address]) 

 if (@sex ='m')
  while @amount> 0
		begin
		    set @name = (select name from [dbo].[name_list_identity] 
			where [name_list_identity].[id] in (select [name_id]  from [dbo].[man_name] 
			where ([dbo].[man_name].[id]= round((rand()*(@m-1) +1),0) )) )

			set @date_of_birth =dateadd(day, (abs(checksum(newid()))%27375), '1925-01-01')
			
		    set @address_id =( select [address_id] from [dbo].[address] 
			where [address_id] = round((rand()*(@address_id_count-1) +1),0) )

            insert into person (surname,name, sex, [date_of_birth],[address_id])
			values (@surname,@name, @sex, @date_of_birth, @address_id)
			set @amount -=1
		end

 else if (@sex ='w')
  while @amount> 0
		begin
		   set @name = (select name from [dbo].[name_list_identity] 
			where [name_list_identity].[id] in (select [name_id]  from [dbo].[woman_name] 
			where ([dbo].[woman_name].[id]= round((rand()*(@w-1) +1),0) )) )

			set @date_of_birth =dateadd(day, (abs(checksum(newid()))%27375), '1925-01-01')
			
		    set @address_id =( select [address_id] from [dbo].[address] 
			where [address_id] = round((rand()*(@address_id_count-1) +1),0) )

			insert into person (surname,name, sex, [date_of_birth],[address_id])
			values (@surname,@name, @sex, @date_of_birth, @address_id)
			set @amount -=1
		end

else if (@sex is null)
  while @amount> 0
	
		begin
			set @name = (select name from [dbo].[name_list_identity] 
			where [name_list_identity].[id] = round((rand()*(@k-1) +1),0) )

			set  @sex = (select sex from [dbo].[name_list_identity] 
			where [dbo].[name_list_identity] .[name]=@name)

			set @date_of_birth =dateadd(day, (abs(checksum(newid()))%27375), '1925-01-01')
