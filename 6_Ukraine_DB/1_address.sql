﻿use [people_ua_db]
go

drop table if exists [person]
go

drop table if exists [men_name]
go

drop table if exists [women_name]
go

drop table if exists [address]
go

drop table if exists [country]
go

drop table if exists [region]
go




create table [country](
[country_id]		int		 identity(1,1),
[country]	    nvarchar(30) null,
constraint PK_country Primary Key ([country_id])
)
go

create table [region](
[region_id]		int		 identity(1,1),
[region]	    nvarchar(30) null,
constraint PK_region Primary Key ([region_id])
)
go

insert into [dbo].[country]
values	('Україна')
go

insert into [dbo].[region]
values	
('Івано-Франківська область'),
('Вінницька область'),
('Волинська область'),
('Дніпропетровська область'),
('Донецька область'),
('Житомирська область'),
('Закарпатська область'),
('Запорізька область'),
('АР Крим'),
('Кіровоградська область'),
('Київська область'),
('Курорт Буковель'),
('Луганська область '),
('Львівська область'),
('Миколаївська область'),
('Одеська область'),
('Полтавська область'),
('Рівненська область'),
('Сумська область'),
('Тернопільська область'),
('Харківська область'),
('Херсонська область'),
('Хмельницька область'),
('Черкаська область'),
('Чернівецька область'),
('Чернігівська область')
go
		



create table [address](
[address_id]		int		 identity(1,1),
[region_id]	        int null,
[country_id]        int null,
constraint PK_address Primary Key ([address_id]),
constraint fk_region foreign key([region_id])
references [dbo].[region]([region_id]),
constraint fk_country foreign key([country_id])
references [dbo].[country]([country_id])
)
go

insert into [dbo].[address] ([country_id],[region_id])
values (1,1),(1,2),(1,3),(1,4),(1,5), (1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),
(1,13),(1,14),(1,15), (1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23), (1,24),
(1,25), (1,26)

