use [people_ua_db]
go


CREATE NONCLUSTERED INDEX [Person_name_surname_index]
ON [people_ua_db].[dbo].[person] (surname, name)


CREATE UNIQUE INDEX [Address_index]
ON [people_ua_db].[dbo].[address] ([region_id], [country_id])
WITH (FILLFACTOR=70, PAD_INDEX= ON)

