USE [triger_fk_cursor]
GO


INSERT INTO street([street])
VALUES ('Doroshenka'),
('Stryiska'),
('Bandery'),
('Soborna'),
('Franka')
GO

INSERT INTO [zone]([name])
VALUES ('heart'),
('lungs'),
('reins'),
('stomach'),
('eyes')
GO




INSERT INTO [post]([post])
VALUES 
('director'),
('deputy director'),
('pharmacist'),
('jr pharmacist')
GO


INSERT INTO [pharmacy](  [name], [building_number], 
[www],  [saturday],  [sunday], [street])          
VALUES 
 ('Znahar', '6', 'www.apteka-zhahar.com.ua',  1, 1, 'Doroshenka'),
 ('3i', '20', 'http://3i.ua/',  0, 0, 'Stryiska'),
 ('DS', '71', 'http://apteka-ds.com.ua/', 1, 1, 'Bandery'),
 ('NSP', '14', 'http://nps.com.ua', 1, 1, 'Soborna'),
 ('Zubytsky', '22', 'www.zubitsky.com.ua',  1, 1, 'Franka'),
 ('Nizkih cen', '37', 'www.aptekanizkihcen.ua',  1, 1, 'Stryiska')
 GO

 INSERT INTO [medicine]([name], ministry_code, recipe, narcotic , psychotropic)  
VALUES 
('nalmefen', 'NO-745-22', 0, 0, 0),
('rylpivin', 'JO-523-05', 1, 0, 0),
('sofosbuvir','JO-515-09', 1, 1, 0),
('fenorol', 'GO-329-03', 0, 1, 0),
('semyprevir', 'JO-543-14', 0, 0, 0),
('dolygretavir', 'JO-457-12', 0, 0, 1),
('izoniazyd', 'JO-497-01', 0, 0, 0),
('treosylfat', 'LO-345-02', 1, 0, 0),
('leveflok', 'JO-123-98', 0, 0, 0),
('dekametoksyn', 'RA-126-32', 0, 0, 0)
GO

INSERT INTO [employee](
    surname, name, midle_name, identity_number, passport,
	experience,  post, pharmacy_id)
VALUES ('Olishchyk', 'Olga', 'Andriivna', '0033456788',  'KC-004596', 5, 'director', 1),
('Kozyra', 'Roman', 'Myroslavovych', '0232678890',  'KC-0456106', 2, 'deputy director', 1),
('Muron', 'Iryna', 'Vasylivna',  '0033456788',  'KC-034567', 1, 'pharmacist', 1),
('Mykyta', 'Mariana', 'Petrivna', '0033456998',  'KC-045678', 10, 'director', 2),
('Pavlyk', 'Pavlo', 'Andrijovych', '0023456788',  'KC-023234', 5, 'pharmacist', 2),
('Vlasov', 'Maria', 'Ihorivna', '0012346543',  'KC-004500', 3, 'director', 3),
('Tymchyk', 'Olga', 'Vasylivna', '0023232323',  'KC-000967', 2, 'pharmacist', 3),
('Senko', 'Olena', 'Maksymivna', '0044444441',  'KC-002389', 1, 'jr pharmacist', 3),
('Maluga', 'Iryna', 'Petrivna', '0088999999',  'KC-011134', 4, 'director', 4),
('Gumen', 'Polina', 'Andriivna', '0021134567',  'KC-024500', 2, 'pharmacist', 4),
('Kryvan', 'Ihor', 'Tarasovych', '0067543214',  'KC-012309', 6, 'pharmacist', 5),
('Politylo', 'Andriy', 'Volodymyrovych', '0003433215',  'KC-009345', 1, 'jr pharmacist', 5),
('Obramych', 'Vasylyna', 'Ihorivna', '0056789423',  'KC-088812', 8, 'director', 6),
('Musko', 'Solomia', 'Vasylivna', '0033003256',  'KC-077732', 5, 'deputy director', 6),
('Popovych', 'Valentyna', 'Andriivna', '0076543297',  'KC-020021', 5, 'pharmacist', 6)
GO


INSERT INTO [pharmacy_medicine](pharmacy_id, medicine_id)
VALUES 
(1,1),(1,2),(1,5), (1,8),
(2,2),(2,4),(2,5),(2,7),
(3,1),(3,6),(3,7),(3,8),
(4,4),(4,5),(4,6),(4,7),(4,8),(4,9),
(5,1),(5,2),(5,3),(5,9),
(6,1),(6,3),(6,5),(6,6),(6,7)

INSERT INTO [medicine_zone](medicine_id, zone_id)
VALUES 
(1,1),
(2,3),
(3,4),
(4,5),
(5,2),
(6,2),
(7,3),
(8,4),
(9,1)

 SELECT * FROM  [medicine]
 SELECT * FROM [pharmacy_medicine]
  
 SELECT * FROM  [medicine]
 SELECT * FROM  [zone]
 SELECT * FROM  [medicine_zone]
 SELECT * FROM [post]
