USE [triger_fk_cursor]
GO

DROP TRIGGER IF EXISTS [tr_pharmacy_street_up_restrict]
GO

DROP TRIGGER IF EXISTS [tr_pharmacy_street_del_restict]
GO

DROP TRIGGER IF EXISTS [tr_pharmacy_street_del_setnull]
GO
--relationship pharmacy and street
CREATE OR ALTER TRIGGER [tr_pharmacy_street_up_restrict]
ON [pharmacy]
AFTER INSERT, UPDATE 
AS 
BEGIN
IF NOT EXISTS
(SELECT [street].[street] FROM [street] inner join inserted on [street].[street]=inserted.[street])
begin
RAISERROR ('This street does not exist in table [street], firstly you need to add this street to table [street]',10,1);
ROLLBACK TRANSACTION 
end
END

--check trigger work
--we can't add pharmacy with street, that doesn't exist in table [street]
INSERT INTO [pharmacy](  [name], [building_number], 
[www], [saturday],  [sunday], [street])          
VALUES 
 ('One', '6', 'www.aptekaone.com.ua', 1, 1, 'Zelena')
GO

CREATE OR ALTER TRIGGER [tr_pharmacy_street_del_restrict]
ON [street]
INSTEAD OF DELETE
AS 
BEGIN
IF EXISTS
(SELECT [street].[street] FROM [street] inner join [pharmacy] on [street].[street]= [pharmacy].[street])
begin
RAISERROR ('You can not delete this street! ',10,1);
ROLLBACK TRANSACTION 
end
END 

--check trigger work
---we can not delete street if there exists pharmacy located on this street
DELETE [street]
WHERE [street].[street]= 'Franka'

