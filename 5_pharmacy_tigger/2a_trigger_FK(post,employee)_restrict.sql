USE [triger_fk_cursor]
GO

drop trigger if exists [tr_post_employee_up_restrict]
go

drop trigger if exists [tr_post_employee_del_restrict]
go

--relationship post and employee 
CREATE OR ALTER TRIGGER [tr_post_employee_up_restrict]
ON [employee]
AFTER INSERT, UPDATE 
AS 
BEGIN
IF NOT EXISTS
(SELECT [post].[post] FROM [post] inner join inserted on [post].[post]=inserted.[post])
begin
RAISERROR ('This post does not exist in table [post], firstly you need to add this post to table [post]',10,1);
ROLLBACK TRANSACTION 
end
END 

--check trigger work
--we can't add employee with post, that doesn't exist in table [post]
INSERT INTO [employee](
    surname, name, midle_name, identity_number, passport,
	experience,  post, pharmacy_id)
VALUES ('Kovalenko', 'Oksana', 'Andriivna', '0033456876',  'KC-004112', 5, 'manager', 1)
GO


CREATE OR ALTER TRIGGER [tr_post_employee_del_restrict]
ON [post]
INSTEAD OF DELETE
AS 
BEGIN
IF EXISTS
(SELECT [post].[post] FROM [post] inner join [employee] on [post].[post]= [employee].[post])
begin
RAISERROR ('You can not delete this post! ',10,1);
ROLLBACK TRANSACTION 
end
END 

--check trigger work
-- we can't delete post, if there is employee who has this post 
DELETE [post]
WHERE [post].[post] ='deputy director'



