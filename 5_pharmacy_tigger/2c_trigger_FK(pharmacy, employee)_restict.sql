USE [triger_fk_cursor]
GO

drop trigger if exists [tr_pharmacy_employee_up_restrict]
go

drop trigger if exists [tr_pharmacy_employee_del_restrict]
go

--relationship pharmacy and employee
CREATE OR ALTER TRIGGER [tr_pharmacy_employee_up_restrict]
ON [employee]
AFTER INSERT, UPDATE 
AS 
BEGIN
IF NOT EXISTS
(SELECT [pharmacy_id] FROM [pharmacy] inner join inserted on [pharmacy].[id]=inserted.[pharmacy_id])
begin
RAISERROR ('This pharmacy does not exist, fisrly you need to add thos pharmacy to table [pharmacy]',10,1);
ROLLBACK TRANSACTION 
end
END

--check trigger work
--we can't add employee with pharmacy, that doesn't exist in table [pharmacy]
INSERT INTO [employee](
    surname, name, midle_name, identity_number, passport,
	experience,  post, pharmacy_id)
VALUES ('Kovalchyk', 'Viktoria', 'Andriivna', '0033400876',  'KC-004111', 5, 'pharmacist', 9)
GO





CREATE OR ALTER TRIGGER [tr_pharmacy_employee_del_restrict]
ON [pharmacy]
INSTEAD OF DELETE
AS 
BEGIN
IF EXISTS
(SELECT [pharmacy].[id] FROM [pharmacy] inner join [employee] on [employee].[pharmacy_id]= [pharmacy].[id])
begin
RAISERROR ('You can not delete this pharmacy!',10,1);
ROLLBACK TRANSACTION 
end
END

--check trigger
--we can't delete pharmacy where exists employee
DELETE [pharmacy]
WHERE [pharmacy].[id]=4

