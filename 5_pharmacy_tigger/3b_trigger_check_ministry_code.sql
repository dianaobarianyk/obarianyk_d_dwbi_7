USE [triger_fk_cursor]
GO

DROP TRIGGER IF EXISTS [tr_medicine_check]
GO

--check ministry code

CREATE OR ALTER TRIGGER [tr_medicine_check]
ON [medicine]
AFTER INSERT, UPDATE
AS 
BEGIN
IF EXISTS 
(SELECT [ministry_code] 
FROM inserted
WHERE [ministry_code] not like '[^MP][^MP]-[0-9][0-9][0-9]-[0-9][0-9]')
begin
RAISERROR ('Ministry code has other format',10,1);
ROLLBACK TRANSACTION 
end
END 

--check trigger
--we can not add ministry code that start with M

INSERT INTO [medicine]([name], ministry_code, recipe, narcotic , psychotropic)  
VALUES 
('nageron', 'ML-123-34', 0, 0, 1)
GO