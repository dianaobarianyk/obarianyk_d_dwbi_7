USE [triger_fk_cursor]
GO

DECLARE @random_number      int
DECLARE @SQLstring1         nvarchar(max)
DECLARE @SQLstring2         nvarchar(max)
DECLARE @SQLstring3         nvarchar(max)
DECLARE @surname            varchar(30)	
DECLARE @name               char(30)   
DECLARE @midle_name         varchar(30)
DECLARE @identity_number    char(10)
DECLARE @passport           char(10)
DECLARE @experience         decimal(10, 1)
DECLARE @birthday           date
DECLARE @post               varchar(15) 
DECLARE @pharmacy_id        int       

SET @SQLstring1= 'CREATE TABLE [table1_' + convert(varchar, GETDATE()) + '] (
    id                 INT IDENTITY PRIMARY KEY,
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
	experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT )'

execute( @SQLstring1)

SET @SQLstring2= 'CREATE TABLE [table2_' + convert(varchar, GETDATE()) + '] (
    id                 INT IDENTITY PRIMARY KEY,
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
	experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT )'

execute( @SQLstring2)

DECLARE T_cursor CURSOR 
FOR SELECT  DISTINCT surname, name, midle_name, identity_number, passport, post
FROM [employee]

OPEN  T_cursor

FETCH NEXT FROM  T_cursor
INTO  @surname, @name, @midle_name, @identity_number, @passport,  @post 
WHILE (@@FETCH_STATUS = 0)
BEGIN
   SET @random_number = convert(int,rand()*(9-1) +1)
   IF @random_number %2 =0
      SET @SQLstring3 =  'INSERT INTO [table1_' + convert(varchar, GETDATE()) + ']( surname, name, midle_name, identity_number, passport, post ) VALUES( '''+  @surname + ''', ''' + @name + ''',''' +  @midle_name + ''', ''' + @identity_number + ''', ''' +  @passport  + ''' , ''' +  @post + ''' )'
   ELSE        SET @SQLstring3 =  'INSERT INTO [table2_' + convert(varchar, GETDATE()) + ']( surname, name, midle_name, identity_number, passport, post ) VALUES( '''+  @surname + ''', ''' + @name + ''',''' +  @midle_name + ''', ''' + @identity_number + ''', ''' +  @passport  + ''' , ''' +  @post + ''' )'
execute( @SQLstring3)

    
  FETCH NEXT FROM  T_cursor
  INTO  @surname, @name, @midle_name, @identity_number, @passport,  @post

END

CLOSE  T_cursor
DEALLOCATE  T_cursor



