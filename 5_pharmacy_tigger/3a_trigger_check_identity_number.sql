USE [triger_fk_cursor]
GO

DROP TRIGGER IF EXISTS [tr_identity_check]
GO

--check identification number 

CREATE OR ALTER TRIGGER [tr_identity_check]
ON [employee]
AFTER INSERT, UPDATE
AS 
BEGIN
IF EXISTS 
(SELECT [identity_number] FROM inserted
WHERE [identity_number] like '%00')
begin
RAISERROR ('Your identity number can not finish with 00',10,1);
ROLLBACK TRANSACTION 
end
END 

--check trigger
--we can't insert employee with identity number finished with '00'
INSERT INTO [employee](
    surname, name, midle_name, identity_number, passport,
	experience,  post, pharmacy_id)
VALUES ('Oliynyk', 'Olena', 'Andriivna', '0033456700',  'KC-004580', 5, 'pharmacist', 1)

--but we can add identity number finished with '0'
INSERT INTO [employee](
    surname, name, midle_name, identity_number, passport,
	experience,  post, pharmacy_id)
VALUES ('Oliynyk', 'Olena', 'Andriivna', '0033456790',  'KC-004580', 5, 'pharmacist', 1)

