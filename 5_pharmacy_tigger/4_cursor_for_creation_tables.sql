USE [triger_fk_cursor]
GO

DECLARE @surname varchar(30), @name varchar(30)
DECLARE @random_number int
DECLARE @SQLstring1 nvarchar(max)
DECLARE @SQLstring2 nvarchar(max)
DECLARE @feature_type nvarchar(30)

DECLARE Employee_cursor CURSOR 
FOR SELECT DISTINCT surname, name
FROM [employee]

OPEN  Employee_cursor

FETCH NEXT FROM  Employee_cursor
INTO @surname, @name
WHILE (@@FETCH_STATUS =0)
BEGIN
  SET @random_number = convert(int,rand()*(9-1) +1)
  SET @feature_type =
   CASE
       WHEN @random_number%2=0 THEN  '  varchar(20),'
	   ELSE ' int, '
    END
  SET @SQLstring1 ='CREATE TABLE ' + @surname +'_'  + @name + ' ('
  SET @SQLstring2 =''
  WHILE @random_number > 0
  begin
   SET @SQLstring2 = @SQLstring2 + 'feature' + convert(varchar,@random_number) 
   +  @feature_type 
   SET    @random_number =  @random_number -1
  end
  SET @SQLstring2 = SUBSTRING(@SQLstring2, 1, LEN(@SQLstring2) -1) + ' )' 
  SET @SQLstring1 =@SQLstring1 + @SQLstring2
   EXECUTE(@SQLstring1)
  
  FETCH NEXT FROM  Employee_cursor
  INTO @surname, @name

END

CLOSE  Employee_cursor
DEALLOCATE  Employee_cursor


