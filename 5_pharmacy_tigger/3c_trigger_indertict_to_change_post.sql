USE [triger_fk_cursor]
GO

DROP TRIGGER IF EXISTS [tr_post]
GO

CREATE OR ALTER TRIGGER [tr_post]
ON [post]
INSTEAD OF UPDATE
AS 
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
IF @ins>0 and @del >0
begin
RAISERROR ('You can not change [post] table',10,1);
ROLLBACK TRANSACTION 
end
END 

--check trigger
--we can not change value of post
UPDATE  [post]
SET [post].[post]= 'head'
WHERE [post].[post]='pharmacist'



