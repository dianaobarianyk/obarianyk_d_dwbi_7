USE [triger_fk_cursor]
GO

drop trigger if exists [tr_pharmacy_medicine_up_restrict]
go

drop trigger if exists [tr_medicine_pharmacy_up_restrict]
go

--relationship pharmacy and medicine
CREATE OR ALTER TRIGGER [tr_pharmacy_medicine_up_restrict]
ON [pharmacy_medicine]
AFTER INSERT, UPDATE 
AS 
BEGIN
IF NOT EXISTS
(SELECT [pharmacy_id] FROM [pharmacy] inner join inserted on [pharmacy].[id]=inserted.[pharmacy_id])
begin
RAISERROR ('This pharmacy does not exist in table [pharmacy]',10,1);
ROLLBACK TRANSACTION 
end
END

--check trigger work
--we can't add pharmacy to [pharmacy_medicine], that doesn't exist in table [pharmacy]
INSERT INTO [pharmacy_medicine]
VALUES (15, 1)
GO

CREATE OR ALTER TRIGGER [tr_medicine_pharmacy_up_restrict]
ON [pharmacy_medicine]
AFTER INSERT, UPDATE 
AS 
BEGIN
IF NOT EXISTS
(SELECT [id] FROM [medicine] inner join inserted on [medicine].[id]=inserted.[medicine_id])
begin
RAISERROR ('This medicine does not exist in table [medicine]',10,1);
ROLLBACK TRANSACTION 
end
END

--check trigger work
--we can't add medicine to [pharmacy_medicine], that doesn't exist in table [medicine]
INSERT INTO [pharmacy_medicine]
VALUES (3, 21)
GO





