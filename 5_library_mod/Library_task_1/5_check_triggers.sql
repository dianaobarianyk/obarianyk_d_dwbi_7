USE [D_Obarianyk_Library]
GO

SELECT * FROM [Library].[Authors]
SELECT * FROM [Library].[Books]
SELECT * FROM [Library].[BooksAuthors]
SELECT * FROM [Library].[Publishers]
SELECT * FROM [Library].[Authors_log]

--check trigger for book to update publishers
INSERT INTO [Library].[Publishers]
([Name], [URL], [inserted], [inserted_by],[country], [City])
VALUES ('Abana','www.abana.com', GETDATE(), user_name(), 'USA','Los Angelos'),
('First Press', 'cup.columbia.edu', GETDATE(), user_name(),'USA', 'NY')
go

INSERT INTO [Library].[Books]
([ISBN], [Publisher_Id], [URL], [title], [edition],[issue],  [Price], [inserted], [inserted_by])
VALUES 
('9780222211113', 9,'www.historyedition.com', 'History stories', 100, 1, 55, GETDATE(), user_name()),
('9785645789234',9, 'www.historyedition2.com', 'History stories 2', 100, 2, 63, GETDATE(), user_name()),
('9780001234521',10, 'www.labormarket.com', 'Market', 50, 1, 52, GETDATE(), user_name())
go

SELECT * FROM [Library].[Publishers]



DELETE [Library].[Books]
WHERE [ISBN]= '9785645789234'

SELECT * FROM [Library].[Publishers]

UPDATE [Library].[Books]
SET [edition]= 300
WHERE [ISBN]= '9780222211113'


SELECT * FROM [Library].[Publishers]

