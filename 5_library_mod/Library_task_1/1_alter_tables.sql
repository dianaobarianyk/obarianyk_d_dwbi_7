USE [D_Obarianyk_Library]
GO

--CREATE SCHEMA [Library] AUTHORIZATION [dbo]
--GO

USE [D_Obarianyk_Library]
GO


ALTER TABLE [Library].[Authors]
ADD [birthday]       [date] NULL,
    [book_amount]    [int]  NOT NULL DEFAULT 0 CHECK([book_amount]>=0),
    [issue_amount]   [int]  NOT NULL DEFAULT 0 CHECK([issue_amount]>=0),
    [total_edition]  [int]  NOT NULL DEFAULT 0 CHECK([total_edition]>=0)
GO


ALTER TABLE [Library].[Publishers]
ADD [created]        [date] NOT NULL DEFAULT '1990.01.01',
	[country]        [varchar](70) DEFAULT 'USA',
	[City]           [varchar](70) DEFAULT 'NY',
    [book_amount]    [int]  NOT NULL DEFAULT 0 CHECK([book_amount]>=0),
    [issue_amount]   [int]  NOT NULL DEFAULT 0 CHECK([issue_amount]>=0),
    [total_edition]  [int]  NOT NULL DEFAULT 0 CHECK([total_edition]>=0)
GO

ALTER TABLE [Library].[Books]
ADD [title]         [varchar](70)   NOT NULL DEFAULT 'Title',
    [edition]       [int]           NOT NULL DEFAULT 1 CHECK([edition]>=1),
    [published]     [date],
    [issue]         [int]           NOT NULL DEFAULT 1        
GO

ALTER TABLE [Library].[BooksAuthors]
ADD 
    [issue]         [int]           NOT NULL DEFAULT 1        
GO


ALTER TABLE [Library].[BooksAuthors]
DROP CONSTRAINT [FK_Books]
GO

ALTER TABLE [Library].[Books]
DROP CONSTRAINT [PK_Bookss_ISBN]
GO

ALTER TABLE [Library].[Books]
ADD CONSTRAINT [PK_Bookss_ISBN_issue]
PRIMARY KEY NONCLUSTERED ([ISBN],[issue])
GO

ALTER TABLE [Library].[BooksAuthors]
ADD CONSTRAINT FK_Books_Authors
FOREIGN KEY([ISBN],[issue])
REFERENCES [Library].[Books]([ISBN], [issue])
ON UPDATE CASCADE



ALTER  TABLE [Library].[Authors_log]
ADD
	[book_amount_old]   [int]			NULL,	
	[issue_amount_old]  [int]			NULL,
	[total_edition_old] [int]			NULL,
	[book_amount_new]	[int]           NULL,
	[issue_amount_new]	[int]			NULL,
	[total_edition_new]	[int]			NULL
GO
