USE [D_Obarianyk_Library]
GO



DROP TRIGGER IF EXISTS [Library].[Books_Authors_tr]
GO

CREATE TRIGGER [Library].[Books_Authors_tr]
ON [Library].[BooksAuthors]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 


if @operation = 'I'
begin

UPDATE [Library].[Authors]
SET 
 [Library].[Authors].[book_amount] = [Library].[Authors].[book_amount] + [Book_count].k,
 [Library].[Authors].[issue_amount] = [Library].[Authors].[issue_amount] + [Book_count].c,
 [Library].[Authors].[total_edition] = [Library].[Authors].[total_edition] + [Book_count].s
 FROM [Library].[Authors]
 inner join  (SELECT [Author_Id], COUNT(*) AS k , COUNT([Library].[Books].[issue]) AS c, SUM([edition]) AS s
              FROM inserted
			  inner join [Library].[Books ] ON  inserted.[ISBN] = [Library].[Books].[ISBN]
			  GROUP BY [Author_Id])
			  AS [Book_count]
			  ON  [Library].[Authors].[Author_Id]= [Book_count].[Author_Id] 
end
if @operation = 'D'
begin

UPDATE [Library].[Authors]
SET 
 [Library].[Authors].[book_amount] = [Library].[Authors].[book_amount] - [Book_count].k,
 [Library].[Authors].[issue_amount] = [Library].[Authors].[issue_amount] - [Book_count].c,
 [Library].[Authors].[total_edition] = [Library].[Authors].[total_edition] - [Book_count].s
 FROM [Library].[Authors]
 inner join  (SELECT [Author_Id], COUNT(*) AS k ,COUNT([Library].[Books].[issue]) AS c, SUM([edition]) AS s
              FROM deleted
			  inner join [Library].[Books ] ON  deleted.[ISBN] = [Library].[Books].[ISBN]
			  GROUP BY [Author_Id])
			  AS [Book_count]
			  ON  [Library].[Authors].[Author_Id]= [Book_count].[Author_Id] 
end


if @operation = 'U'
begin

UPDATE [Library].[Authors]
SET 
 [Library].[Authors].[book_amount] = [Library].[Authors].[book_amount] + [Book_count_i].k - [Book_count_d].k ,
 [Library].[Authors].[issue_amount] = [Library].[Authors].[issue_amount] + [Book_count_i].c - [Book_count_d].c,
 [Library].[Authors].[total_edition] = [Library].[Authors].[total_edition] + [Book_count_i].s - [Book_count_d].s
 FROM [Library].[Authors]
 inner join  (SELECT [Author_Id], COUNT(*) AS k ,COUNT([Library].[Books].[issue]) AS c, SUM([edition]) AS s
              FROM inserted
			  inner join [Library].[Books ] ON  inserted.[ISBN] = [Library].[Books].[ISBN]
			  GROUP BY [Author_Id])
			  AS [Book_count_i]
			  ON  [Library].[Authors].[Author_Id]= [Book_count_i].[Author_Id] 
 inner join  (SELECT [Author_Id], COUNT(*) AS k ,COUNT([Library].[Books].[issue]) AS c,  SUM([edition]) AS s
              FROM deleted
			  inner join [Library].[Books ] ON  deleted.[ISBN] = [Library].[Books].[ISBN]
			  GROUP BY [Author_Id])
			  AS [Book_count_d]
			  ON  [Library].[Authors].[Author_Id]= [Book_count_d].[Author_Id] 
end
END




