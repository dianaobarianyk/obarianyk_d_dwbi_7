USE [D_Obarianyk_Library]
GO

INSERT INTO [Library].[Publishers]
([Name], [URL], [inserted], [inserted_by],[country], [City])
VALUES ('ABC-CLIO','www.abc-clio.com', GETDATE(), user_name(), 'USA','Santa-Barbara'),
('Columbia University Press', 'cup.columbia.edu', GETDATE(), user_name(),'USA', 'NY'),
('CRC Press', 'www.crcpress.com',GETDATE(), user_name(), 'USA', 'NY'),
('Dark Horse Comics', 'www.darkhorse.com', GETDATE(), user_name(), 'USA', 'Milwaukie'),
('Little, Brown and Company', 'www.littlebrown.com', GETDATE(), user_name(), 'USA','NY'),
('Princeton University Press', 'press.princeton.edu',GETDATE(), user_name(), 'USA', 'Princeton'),
('Tokyopop','tokyopop.com', GETDATE(), user_name(), 'USA', 'Los Angeles'),
('University of California Press', 'updress.edu',  GETDATE(), user_name(), 'USA', 'Oakland')
GO

INSERT INTO [Library].[Authors]
([Name], [URL], [inserted], [inserted_by])
VALUES 
('Anne Baltz Rodrick', DEFAULT, GETDATE(), user_name()),
('Mitzi M. Brunsdale', DEFAULT,GETDATE(), user_name()),
('Rimli Bhattacharya', DEFAULT, GETDATE(), user_name()),
('Ronaldo Munck',DEFAULT,GETDATE(), user_name()),
('Dominic Sachsenmaier', DEFAULT,GETDATE(), user_name()),
('Robert Boyers', DEFAULT, GETDATE(), user_name()),
('Sidney Dekker', DEFAULT, GETDATE(), user_name()),
('Louis Theodore', DEFAULT, GETDATE(), user_name()),
('Kelly Behan', DEFAULT, GETDATE(), user_name()),
('Mike Mignola', DEFAULT, GETDATE(), user_name()),
('Troy Nixey', DEFAULT, GETDATE(), user_name()),
('Brian Wood', DEFAULT, GETDATE(), user_name()),
('Justin Giampaoli', DEFAULT, GETDATE(), user_name()),
('Courtenay Hameister', DEFAULT, GETDATE(), user_name()),
('Lucy Tan', DEFAULT, GETDATE(), user_name()),
('Brian OConnor', DEFAULT, GETDATE(), user_name()),
('Orley Ashenfelter', DEFAULT, GETDATE(), user_name()),
('Albert Rees', DEFAULT, GETDATE(), user_name()),
('Rika Tanaka', DEFAULT, GETDATE(), user_name()),
('Renale Lellep Ternandez', DEFAULT, GETDATE(), user_name()),
('Solom Fishman', DEFAULT, GETDATE(), user_name()),
('Rebecca Solnit', DEFAULT, GETDATE(), user_name()),
('Andre Bazin', DEFAULT, GETDATE(), user_name())
GO

INSERT INTO [Library].[Books]
([ISBN], [Publisher_Id], [URL], [title], [edition],[issue],  [Price], [inserted], [inserted_by])
VALUES 
('9780313319686', 1,'www.historyofgreatbritain.com', 'History of Great Britain', 100, 1, 55, GETDATE(), user_name()),
('9781440862748',1, 'www.historyofgreatbritain2.com', 'History of Great Britain, 2', 100, 2, 63, GETDATE(), user_name()),
('9780313306372',1, 'www.studentcompanion.com', 'Student Companion to George', 50, 1, 52, GETDATE(), user_name()),
('9781313333316',1, 'www.gumshoes.com', 'A Dictionary of Detectives', 150, 1, 83,  GETDATE(), user_name()),
('9789382381983',2, 'www.dancingoet.com', 'The Dancing Poet', 200, 1, 40,  GETDATE(), user_name()),
('9781788211055',2, 'www.rethinkingglobal.com', 'Rethinking Global Labor', 100, 1, 30,  GETDATE(), user_name()),
('9780231187527',2, 'www.globalentanglemets.com', 'Global Entanglements', 200, 1, 60,  GETDATE(), user_name()),
('9780231173830',2, 'www.fateofideas.com', 'The Fate of Ideas',100, 1, 25,  GETDATE(), user_name()),
('9780231136747',2, 'www.daictatorsdictation', 'The Dictators Dictation',200, 1, 50,  GETDATE(), user_name()),
('9781315239675',3, 'www.fieldguidetounderstand.com', 'The Field Guide to Understand Error', 300, 1, 20,  GETDATE(), user_name()),
('9781439852255',3, 'www.patientsafety.com','Patient Safety',  150, 1, 25,  GETDATE(), user_name()),
('9781409440604',3, 'www.justculturesafety.com', 'Just Culture: BalancingSafety', 200, 1, 88,  GETDATE(), user_name()),
('9781472475787',3, 'www.justculturetrust.com', 'Just Culture: Restoring Trust', 300, 3, 24,  GETDATE(), user_name()),
('9781138489127',3, 'www.optimazationfor.com', 'Intorduction to Optimization', 50, 1, 80,  GETDATE(), user_name()),
('9781506705446',4, 'www.jennyfinnhc.com', 'Jenny FINN HC', 100, 1, 17,  GETDATE(), user_name()),
('9781506704999',4, 'www.romewest.com', 'Rome West TPB', 300, 1, 15,  GETDATE(), user_name()),
('9780316395700',5, 'www.okayfine.com', 'Okay Fine Whatever', 200, 1, 26,  GETDATE(), user_name()),
('9780316437189',5, 'www.wewerepromised.com', 'What we were promised', 100, 1, 26,  GETDATE(), user_name()),
('9781400889617',6, 'www.idleness.com', 'Idleness', 150, 1, 25,  GETDATE(), user_name()),
('9781400867066',6, 'www.discriminationlabor.com', 'Discrimination in Labor', 200, 1, 30,  GETDATE(), user_name()),
('9781600113425',7, 'www.disneykialaprincess.com', 'Disney Kiala Princess,1', 50, 1, 11,  GETDATE(), user_name()),
('9781600113428',7, 'www.disneykialaprincess2.com' ,'Disney Kiala Princess,2',50, 2, 11,  GETDATE(), user_name()),
('9780520301696',8, 'www.simplematterofsalt.com', 'A Simple matter of Salt', 100, 1, 40,  GETDATE(), user_name()),
('9780520302815',8, 'www.interpretationofart.com', 'Interpretation of Art', 300, 1, 40,  GETDATE(), user_name()),
('9780520262508',8, 'www.infinitecity.com', 'Infinite City', 350, 1, 50,  GETDATE(), user_name()),
('9780520274944',8, 'www.unfathomablecity.com', 'Unfathomable City',  200, 1, 40,  GETDATE(), user_name()),
('9780520242272',8, 'www.whatiscinema.com', 'What is Cinema?',  400, 1, 24,  GETDATE(), user_name())
GO


INSERT INTO [Library].[BooksAuthors]
([BooksAuthors_id],[ISBN],[issue], [Author_Id],[Seq_No], [inserted], [inserted_by])
VALUES 
(1,'9780313319686',1, 1, 1, GETDATE(), user_name()),
(2,'9781440862748',2, 2, 2, GETDATE(), user_name()),
(3,'9789382381983',1, 3, 3, GETDATE(), user_name()),
(4,'9781788211055',1, 4, 4, GETDATE(), user_name()),
(5,'9780231187527',1, 5, 5, GETDATE(), user_name()),
(6,'9780231173830',1, 6, 6, GETDATE(), user_name()),
(7,'9781315239675',1, 7, 7, GETDATE(), user_name()),
(8,'9781439852255',1, 8, 8, GETDATE(), user_name()),
(9,'9781138489127',1, 9, 9, GETDATE(), user_name()),
(10,'9781506705446',1,10, 10, GETDATE(), user_name()),
(11,'9781506704999',1,11, 11, GETDATE(), user_name()),
(12,'9780316395700',1,12, 12, GETDATE(), user_name()),
(13,'9780316437189',1,13, 13, GETDATE(), user_name()),
(14,'9781400889617',1,14, 14, GETDATE(), user_name()),
(15,'9781400867066',1,15, 15, GETDATE(), user_name()),
(16,'9781600113425',1,16, 16, GETDATE(), user_name()),
(17,'9781600113428',2,17, 17, GETDATE(), user_name()),
(18,'9780520301696',1,18, 18, GETDATE(), user_name()),
(19,'9780520302815',1,19, 19, GETDATE(), user_name()),
(20,'9780520262508',1,20, 20, GETDATE(), user_name()),
(21,'9780520242272',1,21, 21, GETDATE(), user_name())
GO





