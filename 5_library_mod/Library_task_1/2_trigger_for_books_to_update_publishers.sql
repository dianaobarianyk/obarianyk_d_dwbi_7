USE [D_Obarianyk_Library]
GO



DROP TRIGGER IF EXISTS [Library].[Books_Publishers]
GO

CREATE TRIGGER [Library].[Books_Publishers]
ON [Library].[Books]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 


if @operation = 'I'
begin

UPDATE [Library].[Publishers]
SET 
 [Library].[Publishers].[book_amount] = [Library].[Publishers].[book_amount] + [Book_count].k,
 [Library].[Publishers].[issue_amount] = [Library].[Publishers].[issue_amount] + [Book_count].c,
 [Library].[Publishers].[total_edition] = [Library].[Publishers].[total_edition] + [Book_count].s
 FROM [Library].[Publishers]
 inner join  (SELECT [Publisher_Id], COUNT(*) AS k , COUNT([issue]) AS c, SUM([edition]) AS s
              FROM inserted
			  GROUP BY [Publisher_Id])
			  AS [Book_count]
			  ON  [Library].[Publishers].[Publisher_Id]= [Book_count].[Publisher_Id] 
end

if @operation = 'D'
begin

UPDATE [Library].[Publishers]
SET 
 [Library].[Publishers].[book_amount] = [Library].[Publishers].[book_amount] - [Book_count].k,
 [Library].[Publishers].[issue_amount] = [Library].[Publishers].[issue_amount] - [Book_count].c,
 [Library].[Publishers].[total_edition] = [Library].[Publishers].[total_edition] - [Book_count].s
 FROM [Library].[Publishers]
 inner join  (SELECT [Publisher_Id], COUNT(*) AS k ,COUNT([issue]) AS c, SUM([edition]) AS s
              FROM deleted
			  GROUP BY [Publisher_Id])
			  AS [Book_count]
			  ON  [Library].[Publishers].[Publisher_Id]= [Book_count].[Publisher_Id] 
end


if @operation = 'U'
begin

UPDATE [Library].[Publishers]
SET 
 [Library].[Publishers].[book_amount] = [Library].[Publishers].[book_amount] + [Book_count_i].k - [Book_count_d].k ,
 [Library].[Publishers].[issue_amount] = [Library].[Publishers].[issue_amount] + [Book_count_i].c - [Book_count_d].c,
 [Library].[Publishers].[total_edition] = [Library].[Publishers].[total_edition] + [Book_count_i].s - [Book_count_d].s
 FROM [Library].[Publishers]
 inner join  (SELECT [Publisher_Id], COUNT(*) AS k ,COUNT([issue]) AS c, SUM([edition]) AS s
              FROM inserted
			  GROUP BY [Publisher_Id])
			  AS [Book_count_i]
			  ON  [Library].[Publishers].[Publisher_Id]= [Book_count_i].[Publisher_Id] 
 inner join  (SELECT [Publisher_Id], COUNT(*) AS k ,COUNT([issue]) AS c,  SUM([edition]) AS s
              FROM deleted
			  GROUP BY [Publisher_Id])
			  AS [Book_count_d]
			  ON  [Library].[Publishers].[Publisher_Id]= [Book_count_d].[Publisher_Id] 
end
END





