USE [D_Obarianyk_Library]
GO

DELETE FROM [Library].[BooksAuthors]
WHERE [BooksAuthors_id]=8

DELETE FROM [Library].[Authors]
WHERE [Name]='Inc. BarCharts'

DELETE FROM [Library].[BooksAuthors]
WHERE [ISBN]='1464143846'

DELETE FROM [Library].[Books]
WHERE [ISBN]='1464143846'

DELETE FROM [Library].[Publishers]
WHERE Name='Worth Publishers'

DELETE FROM [Library].[BooksAuthors]
WHERE [BooksAuthors_id]=3

DELETE FROM [Library].[Authors]
WHERE [URL]='www.billbryson.com'

DELETE FROM [Library].[Books]
WHERE [ISBN]= '076790818X'

DELETE FROM [Library].[Publishers]
WHERE Name='Broadway Books'

--check trigger prevention from deleting
DELETE FROM [Library].[Authors_log]
WHERE [Author_Id_new]=8
