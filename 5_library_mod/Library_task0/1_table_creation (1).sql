USE [D_Obarianyk_Library]
GO
CREATE SCHEMA [Library] AUTHORIZATION [dbo]
GO

USE [D_Obarianyk_Library]
GO

DROP TABLE IF EXISTS [Library].[BooksAuthors]
GO

DROP TABLE IF EXISTS [Library].[Books]
GO

DROP TABLE IF EXISTS [Library].[Authors]
GO

DROP TABLE IF EXISTS [Library].[Publishers]
GO

DROP TABLE IF EXISTS [Library].[Authors_log]
GO

CREATE TABLE [Library].[Authors](
	[Author_Id]	 [int]			NOT NULL IDENTITY,
	[Name]		 [varchar](30)	NOT NULL UNIQUE,
	[URL]		 [varchar](30)	NOT NULL DEFAULT 'www.author_name.com',
	[inserted]	 [datetime]		NOT NULL DEFAULT GETDATE(),
	[inserted_by][varchar](30)	NOT NULL DEFAULT system_user,
	[updated]    [datetime]		NULL,
	[updated_by] [varchar](30)	NULL,
	CONSTRAINT [PK_Authors_id]
	PRIMARY KEY ([Author_id]))
GO


CREATE TABLE [Library].[Publishers](
	[Publisher_Id]	 [int]		NOT NULL IDENTITY,
	[Name]		 [varchar](30)	NOT NULL UNIQUE,
	[URL]		 [varchar](30)	NOT NULL DEFAULT 'www.publisher_name.com',
	[inserted]	 [datetime]		NOT NULL DEFAULT GETDATE(),
	[inserted_by][varchar](30)	NOT NULL DEFAULT system_user,
	[updated]    [datetime]		NULL,
	[updated_by] [varchar](30)	NULL,
	CONSTRAINT   [PK_Publishers_id]
	PRIMARY KEY ([Publisher_id]))
GO

CREATE TABLE [Library].[Books](
	[ISBN]			[varchar](30)	NOT NULL,
	[Publisher_Id]	[int]           NOT NULL,
	[URL]			[varchar](30)	NOT NULL UNIQUE,
	[Price]         [decimal](19,2) NOT NULL DEFAULT 0 CHECK(Price>=0),
	[inserted]		[datetime]		NOT NULL DEFAULT GETDATE(),
	[inserted_by]	[varchar](30)	NOT NULL DEFAULT system_user,
	[updated]		[datetime]		NULL,
	[updated_by]	[varchar](30)	NULL,
	CONSTRAINT [PK_Bookss_ISBN]
	PRIMARY KEY ([ISBN]))
GO

ALTER TABLE [Library].[Books]
ADD CONSTRAINT FK_Books_Publishers
FOREIGN KEY(Publisher_Id)
REFERENCES [Library].[Publishers](Publisher_id)
GO


CREATE TABLE [Library].[BooksAuthors](
	[BooksAuthors_id] [int]         NOT NULL DEFAULT 1 CHECK (BooksAuthors_id >=1),
	[ISBN]			[varchar](30)	NOT NULL UNIQUE,
	[Author_id]	    [int]           NOT NULL UNIQUE,
	[Seq_No]        [int]           NOT NULL DEFAULT 1 CHECK (Seq_No >=1),
	[inserted]		[datetime]		NOT NULL DEFAULT GETDATE(),
	[inserted_by]	[varchar](30)	NOT NULL DEFAULT system_user,
	[updated]		[datetime]		NULL,
	[updated_by]	[varchar](30)	NULL,
	CONSTRAINT [PK_BooksAuthors_id]
	PRIMARY KEY ([BooksAuthors_id]))
GO

ALTER TABLE [Library].[BooksAuthors]
ADD CONSTRAINT FK_Books
FOREIGN KEY(ISBN)
REFERENCES [Library].[Books](ISBN)
ON UPDATE CASCADE

ALTER TABLE [Library].[BooksAuthors]
ADD CONSTRAINT FK_Authors
FOREIGN KEY(Author_Id)
REFERENCES [Library].[Authors](Author_Id)



CREATE TABLE [Library].[Authors_log](
    [operation_id]		[int]           NOT NULL IDENTITY,
	[Author_Id_new]		[int]			NULL,
	[Name_new]			[varchar](30)	NULL,
	[URL_new]			[varchar](30)	NULL,
	[Author_Id_old]		[int]			NULL,
	[Name_old]			[varchar](30)	NULL,
	[URL_old]			[varchar](30)	NULL,
	[operation_type]	[varchar](30)   NOT NULL CHECK (operation_type in ('I','D','U')),
	[operation_datetime][datetime]		NOT NULL DEFAULT GETDATE(),
	CONSTRAINT [PK_Authors_log_id]
	PRIMARY KEY ([operation_id])) 
GO





