USE [D_Obarianyk_Library]
GO

UPDATE [Library].[Authors]
SET [URL]='www.gulbergjan.com'
WHERE [Name]='Jan Gulberg'
GO

UPDATE [Library].[Authors]
SET [Name]='Sylvia Mader'
WHERE [URL]= 'www.sylviamader.com'
GO


UPDATE [Library].[Books]
SET [Price]= 60
WHERE [ISBN]='0078024269'
GO

UPDATE [Library].[Books]
SET [URL]= 'www.economictheory.com'
WHERE [ISBN]= '1464143846'
GO


UPDATE [Library].[Publishers]
SET [URL]='www.booksbasicS.com'
WHERE [Name]='Basics Books'
GO

SELECT * FROM [Library].[Authors]
SELECT * FROM [Library].[Books]
SELECT * FROM [Library].[BooksAuthors]
SELECT * FROM [Library].[Publishers]

SELECT * FROM [Library].[Authors_log]
