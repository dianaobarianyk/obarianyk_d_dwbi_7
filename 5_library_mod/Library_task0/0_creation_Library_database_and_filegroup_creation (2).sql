USE [master]
GO
DROP DATABASE IF EXISTS [D_Obarianyk_Library]
GO

CREATE DATABASE [D_Obarianyk_Library]
GO

USE [master]
GO

ALTER DATABASE [D_Obarianyk_Library]
ADD FILEGROUP [Data]
GO

USE [master]
GO
ALTER DATABASE [D_Obarianyk_Library]
ADD FILE ( NAME = N'D_Obarianyk_Library_data', 
    FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\D_Obarianyk_Library_data.mdf' , 
    SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
GO


USE [D_Obarianyk_Library]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
	ALTER DATABASE [D_Obarianyk_Library]
	MODIFY FILEGROUP [Data] DEFAULT
GO
