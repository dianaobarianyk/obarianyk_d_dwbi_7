﻿USE [labor_sql]
GO

--1
SELECT maker, type FROM [Product]
WHERE model in
(SELECT model FROM [Laptop])
ORDER BY maker 

--2
SELECT model, ram, screen, price FROM [Laptop]
WHERE price > 1000
ORDER BY ram, price DESC

--3
SELECT * FROM [Printer]
WHERE color = 'y'
ORDER BY price DESC

--4
SELECT model, speed,hd, cd, price FROM [PC]
WHERE cd in ('12x','24x') and price <600
ORDER BY speed DESC

--5
SELECT name, class FROM [Ships]
WHERE name in 
(SELECT class FROM [Classes])
ORDER BY name

--6
SELECT [code],[model],[speed], [ram],[hd], [price] FROM [PC]
WHERE speed > = 500 and price < 800
UNION 
SELECT [code],[model],[speed], [ram],[hd], [price] FROM  [Laptop]
WHERE speed > = 500 and price < 800
ORDER BY price DESC

--7
SELECT * FROM [Printer]
WHERE type != 'Matrix 'and price < 300
ORDER BY type DESC 

--8
SELECT model, speed, hd FROM [PC]
WHERE price BETWEEN 400 AND 600
UNION
SELECT model, speed, hd FROM [Laptop]
WHERE price BETWEEN 400 AND 600
ORDER BY hd

--9
SELECT model, speed, hd, price FROM [Laptop]
WHERE screen >= 12
ORDER BY price DESC

--10
SELECT model, type, price FROM [Printer]
WHERE  price <300
ORDER BY type DESC 

--11
SELECT model, ram, price FROM [Laptop]
WHERE ram =64
ORDER BY screen

--12
SELECT model, ram, price FROM [PC]
WHERE ram > 64
ORDER BY hd

--13
SELECT model, speed, price FROM [PC]
WHERE speed BETWEEN 500 and 750
ORDER BY hd DESC

--14
SELECT * FROM [Outcome_o]
WHERE out > 2000
ORDER BY date DESC

--15
SELECT * FROM [Income_o]
WHERE inc BETWEEN 5000 and 10000 
ORDER BY inc

--16
SELECT * FROM [Income]
WHERE point=1
ORDER BY inc

--17
SELECT * FROM [Outcome]
WHERE point=2
ORDER BY out

--18
SELECT * FROM [Classes]
WHERE country ='Japan'
ORDER BY type DESC

--19
SELECT name, launched FROM [Ships]
WHERE launched BETWEEN 1920 and 1942
ORDER BY launched DESC

--20
SELECT ship, battle, result FROM [Outcomes]
WHERE battle ='Guadalcanal' and result != 'sunk'
ORDER BY ship DESC

--21
SELECT ship, battle, result FROM [Outcomes]
WHERE  result = 'sunk'
ORDER BY ship DESC

--22
SELECT class, displacement FROM [Classes]
WHERE displacement > = 40000
ORDER BY type

--23
SELECT trip_no, town_from, town_to FROM [Trip]
WHERE town_from='London' or town_to ='London'
ORDER BY time_out

--24
SELECT trip_no, plane, town_from, town_to FROM [Trip]
WHERE plane='TU-134'
ORDER BY time_out DESC

--25
SELECT trip_no, plane, town_from, town_to FROM [Trip]
WHERE plane !='IL-86'
ORDER BY plane

--26
SELECT trip_no, town_from, town_to FROM [Trip]
WHERE town_from !='Rostov' and town_to != 'Rostov'
ORDER BY plane

--27
SELECT model FROM [PC]
WHERE model LIKE '%1%1%'

--28
SELECT *  FROM [Outcome]
WHERE Month(date) =3

--29
SELECT * FROM [Outcome_o]
WHERE Day(date)=14

--30
SELECT name FROM [Ships]
WHERE name LIKE 'W%n'

--31
SELECT name FROM [Ships]
WHERE name LIKE '%e%e%'

--32
SELECT name, launched FROM [Ships]
WHERE  name NOT LIKE '%a'

--33
SELECT name FROM [Battles]
WHERE name like '% %'and name NOT LIKE '%_c' 

--34
SELECT * FROM [Trip]
WHERE time_out BETWEEN '1900-01-01 12:00:00.000' AND '1900-01-01 17:00:00.000'


--35
SELECT  * FROM [Trip]
WHERE time_in BETWEEN '1900-01-01 17:00:00.000' AND '1900-01-01 23:00:00.000'

--36
SELECT  * FROM [Trip]
WHERE (time_in BETWEEN '1900-01-01 21:00:00.000' AND '1900-01-01 23:59:59.000')
OR (time_in BETWEEN '1900-01-01 00:00:00.000' AND '1900-01-01 10:00:00.000')


--37
SELECT date FROM [Pass_in_trip]
WHERE place LIKE '1%'

--38
SELECT date FROM [Pass_in_trip]
WHERE place LIKE '_c'

--39
SELECT SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)) AS surname
FROM [Passenger]
WHERE name LIKE '% C%'

--40
SELECT SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)) AS surname
FROM [Passenger]
WHERE name NOT LIKE '% J%'

--41
SELECT ('average_price=' + str(AVG(price))) FROM [Laptop]

--42
SELECT ('code = ' + str(code)),
('model = ' + str(model)), 
('speed = ' + str(speed)),
('ram = ' + str(ram)),
('hd = ' + str(hd)),
('cd = ' +  cd),
('price = ' + str(price))
FROM [PC]

--43
SELECT convert(varchar, date, 102) FROM [Income]

--44
SELECT * ,
	CASE 
		WHEN result='OK' THEN 'цілий'
		WHEN result='damaged' THEN 'пошкоджений'
		WHEN result='sunk' THEN 'потоплений'
	END AS result
FROM [Outcomes]

--45
SELECT 'ряд: ' + substring(place, 1 , 1) ,'місце: ' + substring(place, 2 , 2) 
FROM [Pass_in_trip]

--46
SELECT 'from ' + RTRIM(town_from) + ' to ' + town_tO FROM [Trip]

--47
SELECT LEFT(trip_no, 1) + RIGHT(trip_no, 1) 
+ LEFT(id_comp, 1) + RIGHT(id_comp, 1) 
+ LEFT(RTRIM(plane), 1) + RIGHT(RTRIM(plane), 1) 
+ LEFT(RTRIM(town_to), 1) + RIGHT(RTRIM(town_to), 1) 
+ LEFT(RTRIM(town_from), 1) + RIGHT(RTRIM(town_from), 1) 
+ LEFT(RTRIM(time_out), 1) + RIGHT(RTRIM(time_out), 1) 
+ LEFT(RTRIM(time_in), 1) + RIGHT(RTRIM(time_in), 1)
 FROM [Trip]


--48
SELECT maker, count(model)
FROM [Product]
WHERE type = 'PC'  
GROUP BY maker
HAVING COUNT(DISTINCT model) >=2

--49
SELECT town, count(*)  FROM 
(SELECT town_to AS town FROM [Trip] 
UNION ALL
SELECT town_from FROM [Trip])  AS c
GROUP BY town


--50
SELECT type,count(model) FROM [Printer]
GROUP BY type

--51
SELECT  cd,count(DISTINCT model) AS count_model,
model, count(DISTINCT cd) AS count_cd
FROM [PC]
GROUP BY grouping sets(model,cd)



--52
SELECT CAST((time_in - time_out) AS time) FROM [Trip]

--53
SELECT point, date, sum(out) AS total_amount_of_money,
min(out) AS min_amount, max(out) AS max_amount
FROM [Outcome]
GROUP BY ROLLUP(point, date)


--54
SELECT trip_no, substring(place, 1 , 1) AS number_of_row,
COUNT(substring(place, 2 , 2)) AS number_of_place
FROM [Pass_in_trip]
GROUP BY trip_no, substring(place, 1 , 1)
ORDER BY trip_no


--55
SELECT  STR(COUNT(SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)))) 
FROM [Passenger]
WHERE SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name))  LIKE '[ABS]%'
