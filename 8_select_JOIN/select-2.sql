USE [labor_sql]
GO

--1
SELECT PR.maker, PR.type, PC.speed, PC.hd 
FROM [Product] PR
INNER JOIN  PC 
ON PC.model=PR.model
WHERE PC.hd <= 8 ;

--2 
SELECT DISTINCT PR.maker
FROM [Product] PR
INNER JOIN  PC 
ON PC.model=PR.model
WHERE PC.speed >= 600;


--3 
SELECT DISTINCT PR.maker
FROM [Product] PR
INNER JOIN  Laptop L 
ON L.model=PR.model
WHERE L.speed <= 500;


--4 
SELECT  DISTINCT L1.model, L2.model, L1.hd, L1.ram
FROM [Laptop] L1, [Laptop] L2
WHERE L1.hd = L2.hd AND L1.ram = L2.ram AND L1.code! =  L2.code 
AND L1.model >= L2.model;


--5
SELECT C1.country, C1.type, C2.type
FROM Classes C1, Classes C2
WHERE C1.country = C2.country AND  C1.type='bb' AND  C2.type = 'bc';

--6
SELECT DISTINCT PC.model, PR.maker
FROM Product PR
JOIN PC ON PR.model= PC.model
WHERE PC.price < 600;


--7
SELECT DISTINCT PRN.model, PR.maker
FROM Product PR
JOIN Printer  PRN
ON PR.model= PRN.model
WHERE PRN.price > 300;

--8 
SELECT PR.maker, PC.model, PC.price
FROM Product PR
JOIN PC ON PR.model = PC.model
ORDER BY PR.maker, PC.model;

--9 
SELECT PR.maker, PR.model, PC.price
FROM Product PR
LEFT JOIN PC  ON PR.model = PC.model
WHERE type = 'PC'
ORDER BY PR.maker, PC.model;


--10
SELECT PR.maker, PR.type, PR.model, L.speed
FROM Product PR
JOIN Laptop L  ON PR.model = L.model
WHERE L.speed > 600;


--11
SELECT S.name, C.displacement 
FROM Ships S
JOIN Classes C
ON C.class= S.class;

--12
SELECT O.ship, B.name, B.date
FROM Battles B
JOIN Outcomes O
ON B.name= O.battle
WHERE O.result = 'OK';


--13
SELECT S.name, C.country
FROM Ships S
JOIN Classes C
ON S.class = C.class;


--14
SELECT DISTINCT C.name
FROM Trip T
JOIN Company C
ON T.[id_comp] = C.[id_comp]
WHERE T.plane = 'Boeing';

--15 
SELECT P.name, PT.date
FROM Passenger P
JOIN [Pass_in_trip] PT
ON P.[id_psg] = PT.[id_psg]
ORDER BY P.name;


--16
SELECT P.model, L.speed, L.hd
FROM Product P
JOIN Laptop L
ON P.model = L.model
WHERE L.hd IN (10,20) AND P.maker= 'A'
UNION ALL
SELECT P.model, PC.speed, PC.hd
FROM Product P
JOIN PC 
ON P.model = PC.model
WHERE PC.hd IN (10,20) AND P.maker= 'A'
ORDER BY speed;

--17 
SELECT *
FROM (SELECT maker, type, model FROM Product ) AS INFO
PIVOT( COUNT(model) 
FOR type in ([pc], [laptop], [printer])) AS PV;


--18 
SELECT *
FROM (SELECT 'average price'  AS 'avg_' , screen, price FROM Laptop ) AS INFO
PIVOT( AVG(price) 
FOR screen in ([11], [12], [14], [15]) ) AS PV;

--19
SELECT * FROM Laptop L
CROSS APPLY (SELECT  maker FROM Product PR
WHERE PR.model = L.model) PR;


--20 
SELECT * FROM Laptop L1
CROSS APPLY (SELECT  max(price) max_price FROM Laptop L2
JOIN Product PR1 ON L2.model = PR1.model
WHERE maker = (SELECT maker FROM Product PR2 WHERE PR2.model = L1.model )) A;


--21
SELECT * FROM Laptop L1
CROSS APPLY (SELECT  TOP 1 * FROM Laptop L2 
WHERE L1.model < L2.model 
OR (L1.model = L2.model AND L1.code < L2.code)
ORDER BY model, code) A
ORDER BY L1.model;

--22
SELECT * FROM Laptop L1
OUTER APPLY (SELECT  TOP 1 * FROM Laptop L2 
WHERE L1.model < L2.model 
OR (L1.model = L2.model AND L1.code < L2.code)
ORDER BY model, code) A
ORDER BY L1.model;

--23 
SELECT A.* FROM 
(SELECT DISTINCT type FROM Product) PR1
CROSS APPLY (SELECT TOP 3 * FROM Product PR2
WHERE PR1.type =PR2.type
ORDER BY PR2.model) A;


--24 
SELECT code, name, value
FROM Laptop 
CROSS APPLY (
VALUES ('speed', speed),
('ram', ram), ('hd', hd),
('screen', screen) ) spec(name, value)
WHERE code < 4
ORDER BY code, name, value;


